package xivo.xucami.ami

import akka.actor._
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.ManagerResponse
import org.asteriskjava.manager.{ManagerConnection, ManagerConnectionFactory, ManagerEventListener, SendActionCallback}
import play.api.Logger
import play.api.Logger.logger
import services.XucAmiBus
import services.XucAmiBus._
import xivo.xucami.XucAmiConfig
import xivo.xucami.models.{LeaveQueue, EnterQueue}
import xivo.xucami.userevents.{UserEventAgent, UserEventAgentLogin, UserEventAgentLogoff}

import scala.collection.immutable.HashMap
import scala.concurrent.Future
import scala.concurrent.duration._


class AsteriskManagerListener(actor: ActorRef) extends ManagerEventListener with SendActionCallback {
  def onManagerEvent(event: ManagerEvent) = {
    logger.debug(s"Received manager event: $event")
    actor ! event
  }
  def onResponse(response: ManagerResponse) = {
    logger.debug(s"Received manager response: $response")
    actor ! response
  }
}

object ManagerConnector {
  case object Login
  case object LoginTimeout
  type PendingRequests = HashMap[Int, AmiAction]

  class LoginTimeoutException extends Exception

  val managerFactory = new ManagerConnectionFactory(
    XucAmiConfig.ipAddress,
    XucAmiConfig.username,
    XucAmiConfig.secret
  )
  val props = Props(new ManagerConnector(XucAmiBus.amiEventBus, managerFactory))
}


class ManagerConnector(amiBus: XucAmiBus, managerFactory: ManagerConnectionFactory) extends Actor with ActorLogging {
  import ManagerConnector._
  private[ami] var managerConnection: ManagerConnection = null
  private[ami] var loginTimeout: Cancellable = null
  private[ami] var managerListener: AsteriskManagerListener = null
  private[ami] var pendingRequests: PendingRequests = HashMap()
  val loggerAmiRequests = Logger("amirequests")

  override def preStart() {
    amiBus.subscribe(self, AmiType.AmiAction)
    managerListener = new AsteriskManagerListener(self)
    managerConnection = managerFactory.createManagerConnection()
    managerConnection.addEventListener(managerListener)
    managerConnection.registerUserEventClass(classOf[UserEventAgentLogin])
    managerConnection.registerUserEventClass(classOf[UserEventAgentLogoff])

    self ! Login
  }

  def receive = login(List())

  def login(actionsToProcess: List[AmiAction]): Receive = {
    case Login =>
      import scala.concurrent.ExecutionContext.Implicits.global
      loginTimeout = context.system.scheduler.scheduleOnce(5 seconds, self, LoginTimeout)
      Future(managerConnection.login())

    case LoginTimeout =>
      log.error("Manager login timed out")
      throw new LoginTimeoutException

    case event: ManagerEvent =>
      event match {
        case connected: ConnectEvent =>
          log.info("AMI logged on")
          loginTimeout.cancel()
          actionsToProcess.foreach(action => processAction(action))
          context.become(logged)
        case any: Any =>
      }

    case action: AmiAction =>
      log.debug(s"Buffering action to process once logged: $action")
      context.become(login(actionsToProcess :+ action))

    case any =>
      log.warning(s"Receive/login: Unprocessed message received: $any")
  }

  def logged: Receive = {
    case event: ManagerEvent =>
      processEvent(event)

    case action: AmiAction =>
      processAction(action)

    case response: ManagerResponse =>
      processResponse(response)

    case any =>
      log.warning(s"Receive/logged: Unprocessed message received: $any")
  }

  override def postStop() {
    try { managerConnection.logoff() }
    catch { case any: Throwable => log.error(s"Exception in postStop: $any")}
    managerListener = null
    managerConnection = null
  }

  private def processEvent(event: ManagerEvent) {
    event match {
      case agentConnect: AgentConnectEvent =>
        log.debug(s"Publishing AmiEvent: ${agentConnect.getBridgedChannel}, $agentConnect")
        amiBus.publish(AmiEvent(agentConnect))

      case agentEvent: UserEventAgent =>
        log.debug(s"Publishing UserEventAgent: $agentEvent")
        amiBus.publish(AmiUserEventAgent(agentEvent))

      case joinEvent: JoinEvent =>
        log.debug(s"Transforming and publishing a JoinEvent: $joinEvent")
        amiBus.publish(EnterQueue(joinEvent))

      case leaveEvent: LeaveEvent =>
        log.debug(s"Transforming and publishing a JoinEvent: $leaveEvent")
        amiBus.publish(LeaveQueue(leaveEvent))

      case _ =>
        log.debug(s"Publishing unprocessed AmiEvent: $event")
        amiBus.publish(AmiEvent(event))
    }
  }

  private def processResponse(response: ManagerResponse) {
    val actionId = response.getActionId.toInt
    loggerAmiRequests.debug(s"Publishing AMI Response: $response with actionId: $actionId}")
    val richResponse: XucManagerResponse = (response, pendingRequests.get(actionId))
    pendingRequests -= actionId
    amiBus.publish(AmiResponse(richResponse))
  }

  private def processAction(action: AmiAction) {
    val actionId = pendingRequests.keys.lastOption.getOrElse(0) + 1
    pendingRequests += actionId -> action
    action.message.setActionId(actionId.toString)
    loggerAmiRequests.debug(s"Sending action to AMI: $action with actionId: $actionId")
    managerConnection.sendAction(action.message, managerListener)
  }

}




