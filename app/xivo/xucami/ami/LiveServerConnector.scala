package xivo.xucami.ami

import java.beans.PropertyChangeEvent

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import org.asteriskjava.live._
import xivo.xucami.XucAmiConfig
import xivo.xucami.ami.LiveServerConnector.LiveServerInitializationFailedException
import xivo.xucami.models.Channel

import scala.concurrent.Future
import scala.util.{Failure, Success}

object LiveServerConnector {

  class LiveServerInitializationFailedException extends Exception

  val defaultAsteriskServer = new DefaultAsteriskServer(
    XucAmiConfig.ipAddress,
    XucAmiConfig.username,
    XucAmiConfig.secret
  )
  def props(channelManager: ActorRef) = Props(new LiveServerConnector(channelManager, defaultAsteriskServer))
}

class LiveServerConnector(channelManager: ActorRef, asteriskServer: AsteriskServer = null) extends Actor with ActorLogging {

  override def preStart() {
    import scala.concurrent.ExecutionContext.Implicits.global
    val init = Future{ asteriskServer.initialize() }
    init onComplete {
      case Success(_) =>
        log.debug("LiveServer successfully initialized")
        asteriskServer.addAsteriskServerListener(new AsteriskLiveListener(self))
      case Failure(_) =>
        log.error("LiveServer initialization failed")
        throw new LiveServerInitializationFailedException
    }
  }

  def receive = {

    case channel: Channel =>
      log.debug(s"Received channel: $channel")
      channelManager ! channel

    case propertyChange: PropertyChangeEvent =>
      log.debug(s"Received propertyChangeEvent: $propertyChange")
      channelManager ! propertyChange

    case any =>
      log.warning(s"Received unprocessed message: $any")

  }

  override def postStop() {
    try { asteriskServer.shutdown() }
    catch { case any: Throwable => log.error(s"Exception in postStop: $any")}
  }


}
