package xivo.xucami

import play.api.Play.current

object XucAmiConfig {
  val prefix = "xucami"
  val enabled: Boolean = current.configuration.getBoolean(s"$prefix.enabled").getOrElse(true)
  val ipAddress: String = current.configuration.getString(s"$prefix.amiIpAddress").getOrElse("127.0.0.1")
  val port: Int = current.configuration.getInt(s"$prefix.amiPort").getOrElse(5038)
  val username: String = current.configuration.getString(s"$prefix.username").getOrElse("xuc")
  val secret: String = current.configuration.getString(s"$prefix.secret").getOrElse("8uc53c837")

  def metricsRegistryName: String = current.configuration.getString("techMetrics.name").getOrElse("techMetrics")
  def metricsLogReporter: Boolean = current.configuration.getBoolean("techMetrics.logReporter").getOrElse(true)
  def metricsLogReporterPeriod: Long = current.configuration.getLong("techMetrics.logReporterPeriod").getOrElse(5)

}
