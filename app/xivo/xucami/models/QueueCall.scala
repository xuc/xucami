package xivo.xucami.models

import org.asteriskjava.manager.event.{JoinEvent, LeaveEvent}
import org.joda.time.DateTime
import services.XucAmiBus
import services.XucAmiBus.AmiMessage

case class QueueCall(position: Int, name: String, number: String, queueTime: DateTime)

object QueueCall {
  def apply(event: JoinEvent): QueueCall = QueueCall(event.getPosition, event.getCallerIdName, event.getCallerIdNum, new DateTime(event.getDateReceived))
}

case class EnterQueue(queue: String, uniqueId: String, queueCall: QueueCall) extends AmiMessage {
  override val message: Any = this
  override val classifier: String = XucAmiBus.AmiType.QueueEvent
}

object EnterQueue {
  def apply(event: JoinEvent): EnterQueue = EnterQueue(event.getQueue, event.getUniqueId, QueueCall(event))
}

case class LeaveQueue(queue: String, channelName: String) extends AmiMessage {
  override val message: Any = this
  override val classifier: String = XucAmiBus.AmiType.QueueEvent
}

object LeaveQueue {
  def apply(event: LeaveEvent): LeaveQueue = LeaveQueue(event.getQueue, event.getUniqueId)
}