package xivo.xucami.models

import org.asteriskjava.live.AsteriskChannel
import org.asteriskjava.manager.event.{AgentCalledEvent, NewChannelEvent, NewStateEvent}
import collection.JavaConversions._

case class CallerId(name: String, number: String) {
  def isNotFullySet = this.name == "" || this.name == null || this.number == "" || this.number == null
}

object ChannelState extends Enumeration(-2) {
  type ChannelState = Value
  val UNITIALIZED, HUNGUP, DOWN, RSRVD, OFFHOOK, DIALING, ORIGINATING, RINGING, UP, BUSY, DIALING_OFFHOOK, PRERING = Value
}
import ChannelState.ChannelState

object ChannelDirection extends Enumeration {
  type ChannelDirection = Value
  val UNKNOWN, ORIGINATING, TERMINATING = Value
}

object MonitorState extends Enumeration {
  type MonitorState = Value
  val DISABLED, ACTIVE, PAUSED, UNKNOWN = Value
}
import MonitorState.MonitorState

object Channel {
  val ZombieMark = "ZOMBIE"
  val AgentCallBackMark = "agentcallback"
  val OtherEndUp = Map("XUC_OTHER_END_UP"-> "1")
  val OutBoundOtherEnd = Map("XUC_OUTBOUND"-> "OtherEnd")
  val Masquerade = Map("Masquerade"->"1")

  def getChanId(astChannel: AsteriskChannel) = astChannel.getLinkedChannel match {
    case linkedChan: AsteriskChannel =>
      linkedChan.getId
    case _ =>
      astChannel.getId
  }

  def apply(astChannel: AsteriskChannel): Channel = {
    Channel(
      astChannel.getId,
      astChannel.getName,
      CallerId(astChannel.getCallerId.getName, astChannel.getCallerId.getNumber),
      linkedChannelId = getChanId(astChannel),
      ChannelState.apply(astChannel.getState.getStatus),
      variables = astChannel.getVariables.toMap
    )
  }

  def apply(newChanEvent: NewChannelEvent): Channel = {
    def getState(s:Integer) = if (s != null) ChannelState(s) else ChannelState.UNITIALIZED
    Channel(
      newChanEvent.getUniqueId,
      newChanEvent.getChannel,
      CallerId(newChanEvent.getCallerIdName, newChanEvent.getCallerIdNum),
      exten = propToString(newChanEvent.getExten),
      state = getState(newChanEvent.getChannelState),
      linkedChannelId = newChanEvent.getUniqueId)
  }
  private def propToString(s:String):Option[String] = if (s != null) Some(s) else None
}

case class Channel(
  val id: String,
  val name: String,
  val callerId: CallerId,
  val linkedChannelId: String,
  var state: ChannelState = ChannelState.UNITIALIZED,
  var monitored: MonitorState = MonitorState.DISABLED,
  val exten: Option[String] = None,
  val connectedLineNb: Option[String] = None,
  var agentNumber: Option[String] = None,
  val variables: Map[String,String] = Map()
) {
  def updateVariable(name: String, value: String): Channel = this.copy(variables = this.variables + (name->value))
  def addVariables(newVariables : Map[String, String]): Channel = this.copy(variables = this.variables ++ newVariables )
  def withLinkedId(channelId: String):Channel = this.copy(linkedChannelId = channelId)
  def withCallerId(newCallerId: CallerId): Channel =  if (this.callerId.isNotFullySet)  this.copy(callerId = newCallerId)  else this
  def withChannelState(newState:ChannelState):Channel = this.copy(state = newState)
  def withState(newStEvt: NewStateEvent):Channel = {
    if (this.isAgentCallback)
      this.copy(state = ChannelState(newStEvt.getChannelState),callerId=CallerId(newStEvt.getCallerIdName, newStEvt.getCallerIdNum), connectedLineNb=Channel.propToString(newStEvt.getConnectedLineNum))
    else
      this.copy(state = ChannelState(newStEvt.getChannelState), connectedLineNb=Channel.propToString(newStEvt.getConnectedLineNum))
  }

  def withAgentName(memberName: String): Channel = this.copy(agentNumber= memberName.split("/").lastOption)

  def isAgentCallback():Boolean = this.name.contains(Channel.AgentCallBackMark)

}

object ChannelAction extends Enumeration {
  type ChannelAction = Value
  val PauseMonitor, UnpauseMonitor = Value
}

import ChannelAction.ChannelAction

case class ChannelActionRequest(
  val agentNumber: Option[String],
  val action: ChannelAction
)


