package xivo.xucami

import akka.actor.SupervisorStrategy.{Restart, Stop, Decider}
import akka.actor._
import services.AmiRequestManager
import services.channel.ChannelManager
import xivo.xucami.ami.{LiveServerConnector, ManagerConnector}
import scala.concurrent.duration._

object AmiSupervisor {
  val props = Props(new AmiSupervisor())
}

class AmiSupervisor extends Actor with ActorLogging {

  val ManagerConnectorName = "XucAmiManagerConnector"
  val ChannelManagerName = "XucAmiChannelManager"
  val SetVarManagerName = "XucAmiSetVarManager"
  val LiveServerConnectorName = "XucAmiLiveServerConnector"

  override def preStart() = {
    context.actorOf(ManagerConnector.props, ManagerConnectorName)
    val channelManager = context.actorOf(ChannelManager.props, ChannelManagerName)
    context.actorOf(AmiRequestManager.props, SetVarManagerName)
    context.actorOf(LiveServerConnector.props(channelManager), LiveServerConnectorName )
  }

  def decider: Decider = {
    case _: Exception ⇒ Restart
  }

  override def supervisorStrategy: SupervisorStrategy = AllForOneStrategy(-1, Duration.Inf, true)(decider)

  def receive = {
    case any =>
      log.info(s"Not processing $any")
  }
}
