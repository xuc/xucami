package services.channel

import play.api.Logger
import services.channel.ChannelManager.Channels
import xivo.xucami.models.Channel

import scala.collection.immutable.HashMap

case class ChannelRepository(channels:Channels) {
  val log = Logger(ChannelRepository.getClass.getName)

  def addNewChannel(channel: Channel): ChannelRepository = channels.get(channel.id) match {
    case None =>
      log.debug(s"Adding New C hannel : $channel")
      ChannelRepository(channels + (channel.id -> channel))
    case _ =>
      log.debug(s"Channel : $channel already exists")
      this
  }
  def updateChannelsLinkedId(oldLinkedId:String, newLinkedId:String):Channels = {
    channels.map {
        case (id, channel) if (channel.linkedChannelId == oldLinkedId) => (id -> channel.withLinkedId(newLinkedId))
        case (id, channel) => (id, channel)
    }
  }

  def updateLinkedId(fromId:String, toId:String):ChannelRepository = {
    val chans = for {
      initChan:Channel <- channels.get(fromId)
      destChan:Channel <- channels.get(toId)
    } yield({
        val upChans = updateChannelsLinkedId(destChan.linkedChannelId, initChan.linkedChannelId)
        upChans + (destChan.id -> destChan.withLinkedId(initChan.linkedChannelId))
      })

    ChannelRepository(chans.getOrElse(channels))
  }

  def updateChannel(channel: Channel, f: Channel => Channel = (c:Channel)=>c) = ChannelRepository(channels + (channel.id -> f(channel)))

  def propagateVars(fromId1: String, toId2: String):ChannelRepository = {
    log.debug(s"prop vars $channels from $fromId1 to $toId2")
    def update(from:Channel, to:Channel):Map[String,String] = to.variables ++ from.variables
    val chans = for {
      fromChannel <- channels.get(fromId1)
      toChannel <- channels.get(toId2)
    } yield(channels + (toChannel.id -> toChannel.copy(variables = update(fromChannel, toChannel))))

    log.debug(s"variables updated ${channels.get(toId2)}")
    ChannelRepository(chans.getOrElse(channels))
  }
}
