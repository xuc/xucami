package services.channel

import org.asteriskjava.manager.event._
import play.api.Logger
import services.XucAmiBus
import services.XucAmiBus._
import services.channel.ChannelManager.Channels
import xivo.xucami.models.{ChannelState, Channel, CallerId}

  trait EventProcessor {
    val processor: AmiEventProcessor
  }

  class AmiEventProcessor(amiBus:XucAmiBus) {
    val log = Logger(this.getClass.getName)
    val loggerAmi = Logger("amievents")

    def process( event:AmiEvent, channelRepo:ChannelRepository):Channels = {
      event.message match {

        case agentCalled: AgentCalledEvent =>
          processAgentCalled(agentCalled, channelRepo.channels)

        case agentConnect: AgentConnectEvent =>
          processAgentConnect(agentConnect, channelRepo.channels)

        case hangup: HangupEvent =>
          val result = processHangup(hangup, channelRepo.channels)
          log.debug(s"Channels after hangup: $result")
          result

        case spyStarted: ChanSpyStartEvent =>
          log.debug(s"spyee ${spyStarted.getSpyeeChannel}")
          log.debug(s"spyer ${spyStarted.getSpyerChannel}")
          for {
            spyeeChannel <- channelRepo.channels.values.find(_.name == spyStarted.getSpyeeChannel)
            spyerChannel <- channelRepo.channels.values.find(_.name == spyStarted.getSpyerChannel)
          } {
            log.debug(s"spy started : spyee: $spyeeChannel <-> spyer : $spyerChannel")
            amiBus.publish(SpyStarted(SpyChannels(spyerChannel,spyeeChannel)))
          }
          channelRepo.channels
        case spyStopped: ChanSpyStopEvent =>
          log.debug(s"spy stopped: $spyStopped")
          channelRepo.channels.values.find(_.name== spyStopped.getSpyeeChannel) foreach { channel =>
            amiBus.publish(SpyStopped(channel))
          }
          channelRepo.channels

        case newState : NewStateEvent =>
          channelRepo.channels.get(newState.getUniqueId).map {
            channel =>
              val chans = channelRepo.updateChannel(channel.withState(newState)).propagateVars(channel.linkedChannelId, channel.id).channels
              publish(chans(channel.id))
              if (channel.linkedChannelId != channel.id) {
                if(chans(channel.id).state == ChannelState.UP) {
                  chans.get(channel.linkedChannelId).map {
                    linkedChannel =>
                      publish(linkedChannel.addVariables(Channel.OtherEndUp))
                  }
                }
              }
              chans
          }.getOrElse(channelRepo.channels)

        case rename: RenameEvent =>
          channelRepo.channels.get(rename.getUniqueId).map ( channel => channelRepo.channels + (channel.id -> channel.copy(name=rename.getNewname))).getOrElse(channelRepo.channels)

        case varSetEvent: VarSetEvent if(varSetEvent.getValue != null) =>
          var chans = channelRepo.channels.get(varSetEvent.getUniqueId).map ( channel => channelRepo.channels + (channel.id -> channel.updateVariable(varSetEvent.getVariable, varSetEvent.getValue))).getOrElse(channelRepo.channels)
          log.debug(s">> ${chans.get(varSetEvent.getUniqueId)}")
          chans

        case localBridgeEvent: LocalBridgeEvent =>
          var chans = channelRepo.propagateVars(localBridgeEvent.getUniqueId1, localBridgeEvent.getUniqueId2).channels
          val chans2  = for {
            chan1 <- chans.get(localBridgeEvent.getUniqueId1)
            chan2 <- chans.get(localBridgeEvent.getUniqueId2)
          }  yield(chans + (chan2.id -> chan2.withLinkedId(chan1.linkedChannelId)))
          chans2.getOrElse(chans)

        case newCallerId : NewCallerIdEvent =>
          channelRepo.channels.get(newCallerId.getUniqueId).map(channel => channelRepo.channels + (channel.id -> channel.withCallerId(CallerId(newCallerId.getCallerIdName, newCallerId.getCallerIdNum)))).getOrElse(channelRepo.channels)

        case dialEvent: DialEvent =>
          val chans = for {
            initChan:Channel <- channelRepo.channels.get(dialEvent.getUniqueId)
            destChan:Channel <- channelRepo.channels.get(dialEvent.getDestUniqueId)
          } yield(channelRepo.channels + (destChan.id -> destChan.withLinkedId(initChan.linkedChannelId).addVariables(Channel.OutBoundOtherEnd)))
          chans.getOrElse(channelRepo.channels)

        case masquerade : MasqueradeEvent =>
          var chans = for {
            original <- channelRepo.channels.values.find(_.name == masquerade.getOriginal)
            clone <- channelRepo.channels.values.find(_.name == masquerade.getClone)
          } yield{
              if (original.callerId != clone.callerId)  publish(original.withChannelState(ChannelState.HUNGUP))
              val newOrigin = original.copy(callerId = clone.callerId, state=clone.state)
              publish(newOrigin.addVariables(Channel.Masquerade))
              channelRepo.channels + (original.id -> newOrigin)
            }
          chans.getOrElse(channelRepo.channels)

        case any =>
          log.debug(s"Unprocessed ami event $any")
          channelRepo.channels
      }
    }
    private def processAgentCalled(event: AgentCalledEvent, channels: Channels): Channels = {

      log.debug(s"processing agent called $event")

      def updateOrCreateChannel():Channel = channels.get(event.getUniqueId) match {
        case Some(foundChannel) => foundChannel withAgentName(event.getAgentName)
        case None => Channel(
          event.getUniqueId,
          event.getDestinationChannel,
          CallerId(event.getCallerIdName, event.getCallerIdNum),
          event.getUniqueId
        ) withAgentName(event.getAgentName)
      }

      def updateDestinationChannel(sourceChannel: Channel, destName: String) = if (destName != null) {

        def updateLinkedId(destChannel:Channel, linkId: String): Channels = {
          val upChans:Channels = channels.filter(_._2.linkedChannelId == destChannel.linkedChannelId) map {
            case((id, bridgedChan)) => (id ->  bridgedChan.withLinkedId(linkId))
          }
          channels ++ upChans
        }
        channels.find(_._2.name == destName).headOption match {
          case Some((id, destChan)) =>
            log.debug(s"update channel variables  $id with ${sourceChannel.variables}")
            val chans = updateLinkedId(destChan, sourceChannel.linkedChannelId)
            val chan = destChan.addVariables(sourceChannel.variables).withLinkedId(sourceChannel.linkedChannelId)
            chans + (destChan.id -> chan)
          case _ => channels
        }
      }
      else channels

      val agentChannel = updateOrCreateChannel
      publish(agentChannel)
      updateDestinationChannel(agentChannel, event.getDestinationChannel) + (agentChannel.id -> agentChannel)
    }
    private def processAgentConnect(event: AgentConnectEvent, channels: Channels): Channels = {
      channels.get(event.getUniqueId) match {
        case Some(channel) =>
          log.debug(s"Channel $channel answered by ${event.getMemberName}")
          val chan = channel.withAgentName(event.getMemberName)
          publish(chan)
          channels  + (event.getUniqueId -> chan)
        case None =>
          log.debug("Got an AgentConnect event for unknown channel.")
          channels
      }
    }
    private def processHangup(event: HangupEvent, channels: Channels): Channels = {
      channels.get(event.getUniqueId) match {
        case Some(channel) =>
          log.debug(s"Channel $channel hangup")
          channel.state = ChannelState.HUNGUP
          if (!channel.name.contains(Channel.ZombieMark)) publish(channel)
          channels - event.getUniqueId
        case None =>
          log.debug("Got a Hangup event for unknown channel.")
          channels
      }
    }

    private def publish(channel:Channel) = {
      loggerAmi.debug(s"Pub: $channel")
      amiBus.publish(ChannelEvent(channel))
    }

  }

