package services.channel

import org.asteriskjava.manager.event._
import play.api.Logger
import services.XucAmiBus.AmiEvent

object AmiLogger {
  val log = Logger("amievents")
  def prChan(f:String) = log.info(s"Chan-> $f")
  def pIds(evt: AbstractChannelEvent):String = s"${evt.getUniqueId} ${evt.getChannel}"
  def cId(me: ManagerEvent):String = s"Cid(${me.getCallerIdName},${me.getCallerIdNum})"

  def logEvent(event: AmiEvent) = {

    event.message match {
      case nce: NewChannelEvent         => prChan(s"NewChannelEvent    : ${pIds(nce)} ${nce.getChannelStateDesc} ${nce.getExten}")
      case nci: NewCallerIdEvent        => prChan(s"NewCallerIdEvent   : ${pIds(nci)} ${cId(nci)}")
      case lcb: LocalBridgeEvent        => prChan(s"LocalBridgeEvent   : ${lcb.getUniqueId1} ${lcb.getUniqueId2}")
      case nse: NewStateEvent           => prChan(s"NewStateEvent      : ${pIds(nse)} ${nse.getChannelStateDesc} ${cId(nse)} ${nse.getConnectedLineNum}")
      case ace: AgentCalledEvent        => prChan(s"AgentCalledEvent   : ${ace.getUniqueId} ${ace.getAgentName} ${ace.getQueue} ${cId(ace)} cal:${ace.getChannelCalling} dest:${ace.getDestinationChannel} ${ace.getConnectedLineNum}")
      case dia: DialEvent               => prChan(s"DialEvent          : ${dia.getUniqueId} ${dia.getChannel} ${dia.getSubEvent} ${dia.getDestUniqueId}")
      case bri: BridgeEvent             => prChan(s"BridgeEvent        : ${bri.getUniqueId1} ${bri.getUniqueId2} ${bri.isLink}")
      case aco: AgentConnectEvent       => prChan(s"AgentConnectEvent  : ${aco.getUniqueId} ${aco.getChannel} ${aco.getMemberName} ${aco.getQueue} ${aco.getBridgedChannel}")
      case mae: MasqueradeEvent         => prChan(s"MasqueradeEvent    : clone[${mae.getClone}] original[${mae.getOriginal}]")
      case ren: RenameEvent             => prChan(s"RenameEvent        : ${ren.getUniqueId} ${ren.getChannel} ${ren.getNewname}")
      case hur: HangupRequestEvent      => prChan(s"HangupRequestEvent : ${pIds(hur)}")
      case hue: HangupEvent             => prChan(s"HangupEvent        : ${pIds(hue)} ${hue.getConnectedLineNum} ${hue.getCause}|${hue.getCauseTxt}")
      case soh: SoftHangupRequestEvent  => prChan(s"SoftHgupRequestEv" +
        s"t : ${pIds(soh)} ${soh.getCause}")
      case any =>
    }
  }
}
