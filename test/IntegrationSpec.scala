import akka.actor.{Props, Actor}
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.asteriskjava.manager.action.CoreStatusAction
import play.api.test.FakeApplication
import play.api.test.Helpers.running
import services.XucAmiBus
import services.XucAmiBus.{AmiType, AmiAction, AmiResponse}
import xivo.xucami.ami.ManagerConnector
import scala.concurrent.duration._

class IntegrationSpec extends TestKitSpec("Integration") {

  val testConfig: Map[String, Any] = {
    Map(
      "logger.root" -> "INFO",
      "logger.application" -> "INFO",
      "logger.services" -> "INFO",
      "akka.loggers" -> List("play.api.Logger"),
      "log-dead-letters-during-shutdown" -> "on",
      "loglevel" -> "ERROR",
      "xucami.amiIpAddress" -> "xivo-integration",
      "xucami.amiPort" -> "5038",
      "xucami.username" -> "xuc",
      "xucami.secret" -> "xucpass")
  }

  override def afterAll {
    system.shutdown()
  }

  "Application" should(

    "be able to connect to AMI and get CoreStatus" in {
      running(FakeApplication(additionalConfiguration = testConfig)) {
        val probe = TestProbe()
        val amiBus = XucAmiBus.amiEventBus
        val manager = system.actorOf(ManagerConnector.props)
        system.actorOf(Props(new Actor() {
          amiBus.subscribe(self, AmiType.separator)

          def receive = {
            case response: AmiResponse =>
              probe.ref ! response
          }
        }))
        manager ! AmiAction(new CoreStatusAction())
        probe.expectMsgClass(5 seconds, classOf[AmiResponse])
      }
    }
  )
}
