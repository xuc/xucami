package xivo.xucami.models

import org.asteriskjava.manager.event.{JoinEvent, LeaveEvent}
import org.joda.time.DateTime
import org.mockito.Mockito.stub
import org.scalatest.mock.MockitoSugar
import org.scalatest.{Matchers, WordSpec}

class QueueCallSpec  extends WordSpec with Matchers with MockitoSugar {

  "A QueueCall" should {
    "be created from a JoinEvent" in {
      val event = mock[JoinEvent]
      val position = 3
      val dateJoined = new DateTime()
      val name = "foo"
      val number = "3315987"
      stub(event.getCallerIdName).toReturn(name)
      stub(event.getCallerIdNum).toReturn(number)
      stub(event.getPosition).toReturn(position)
      stub(event.getDateReceived).toReturn(dateJoined.toDate)

      QueueCall(event) shouldEqual QueueCall(position, name, number, dateJoined)
    }
  }

  "An EnterQueue" should {
    "be created from a JoinEvent" in {
      val event = mock[JoinEvent]
      val position = 3
      val dateJoined = new DateTime()
      val name = "foo"
      val number = "3315987"
      val queue = "bar"
      val uniqueid = "123456.789"
      stub(event.getCallerIdName).toReturn(name)
      stub(event.getCallerIdNum).toReturn(number)
      stub(event.getPosition).toReturn(position)
      stub(event.getDateReceived).toReturn(dateJoined.toDate)
      stub(event.getQueue).toReturn(queue)
      stub(event.getUniqueId).toReturn(uniqueid)

      EnterQueue(event) shouldEqual EnterQueue(queue, uniqueid, QueueCall(position, name, number, dateJoined))
    }
  }

  "A LeaveQueue" should {
    "be created from a LeaveEvent" in {
      val event = mock[LeaveEvent]
      val queue = "bar"
      val uniqueId = "12456.789"
      stub(event.getQueue).toReturn(queue)
      stub(event.getUniqueId).toReturn(uniqueId)

      LeaveQueue(event) shouldEqual LeaveQueue(queue, uniqueId)
    }
  }

}
