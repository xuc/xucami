package xivo.xucami.ami

import akka.actor.{PoisonPill, Cancellable}
import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.mockito.Matchers.isA
import org.asteriskjava.manager.action.{CoreStatusAction, UnpauseMonitorAction, PauseMonitorAction}
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.CoreStatusResponse
import org.asteriskjava.manager.{ManagerConnectionFactory, ManagerConnection}
import org.mockito.Mockito.{when, verify, timeout}
import org.scalatest.mock.MockitoSugar
import services.XucAmiBus
import services.XucAmiBus._
import xivo.xucami.models.{LeaveQueue, EnterQueue}
import xivo.xucami.userevents.UserEventAgent
import scala.collection.immutable.HashMap

class ManagerConnectorSpec extends TestKitSpec("ManagerConnectorSpec")
  with MockitoSugar {

  class Helper() {
    val amiBus = mock[XucAmiBus]
    val factory = mock[ManagerConnectionFactory]
    val connection = mock[ManagerConnection]

    def actor() = {
      when(factory.createManagerConnection()).thenReturn(connection)
      val sa = TestActorRef(new ManagerConnector(amiBus, factory))
      (sa, sa.underlyingActor)
    }
  }

  override def afterAll() {
    system.shutdown()
  }

  "ManagerConnector" should {

    "initiate ManagerConnection" in new Helper() {
      val (ref, a) = actor()
      verify(factory).createManagerConnection()
      a.managerConnection shouldBe connection
      verify(a.managerConnection, timeout(250)).login()
    }

    "stop LoginTimeout if ConnectEvent received" in new Helper() {
      val (ref, a) = actor()
      a.loginTimeout = mock[Cancellable]
      ref ! new ConnectEvent("test")
      verify(a.loginTimeout).cancel()
    }

    "logoff and defer manager connection if the actor is terminated" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("testLogout")
      ref ! PoisonPill

      a.managerConnection shouldBe null
    }

    "publish AgentConnect event to the bus with channel id" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val event = new AgentConnectEvent("agentConnectTest")
      ref ! event

      verify(amiBus).publish(AmiEvent(event))
    }

    "publish unprocessed events to the bus without channel" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val event = new ChannelReloadEvent("unprocessed")
      ref ! event

      verify(amiBus).publish(AmiEvent(event))
    }

    "subscribe to the bus for AmiActions" in new Helper() {
      val (ref, a) = actor()
      verify(amiBus).subscribe(ref, AmiType.AmiAction)
    }

    "send Manager Actions with refence to the ami and save them in pendingRequests with actionId" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val action = mock[PauseMonitorAction]
      ref ! AmiAction(action, Some("uniqueId3"))

      verify(action).setActionId("1")
      verify(connection).sendAction(action, a.managerListener)
      a.pendingRequests.get(1) shouldBe Some(AmiAction(action, Some("uniqueId3")))
    }

    "buffer Manager Actions when waiting for login and send them once logged" in new Helper() {
      val (ref, a) = actor()
      val action1 = new PauseMonitorAction()
      val action2 = new UnpauseMonitorAction()
      ref ! AmiAction(action1)
      ref ! AmiAction(action2)
      ref ! new ConnectEvent("test")

      verify(connection).sendAction(action1, a.managerListener)
      verify(connection).sendAction(action2, a.managerListener)
    }

    "publish Ami responses to the bus with corresponding action" in new Helper() {
      val (ref, a) = actor()
      val action = new CoreStatusAction()
      a.pendingRequests = HashMap(2 -> AmiAction(action))
      val response = new CoreStatusResponse()
      response.setActionId("2")
      a.logged(response)

      a.pendingRequests.get(2) shouldBe None
      verify(amiBus).publish(AmiResponse(response, Some(AmiAction(action))))
    }
    "publish Ami user event agent to bus on usereventagent topic" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val userEventAgent = mock[UserEventAgent]

      ref ! userEventAgent

      verify(amiBus).publish(AmiUserEventAgent(userEventAgent))
    }

    "publish an EnterQueue event on the bus when receiving a JoinEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val joinEvent = mock[JoinEvent]

      ref ! joinEvent

      verify(amiBus).publish(isA(classOf[EnterQueue]))
    }

    "publish a LeaveQueue event on the bus when receiving a LeaveEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val leaveEvent = mock[LeaveEvent]

      ref ! leaveEvent

      verify(amiBus).publish(isA(classOf[LeaveQueue]))
    }
  }
}
