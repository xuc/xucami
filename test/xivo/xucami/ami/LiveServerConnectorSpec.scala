package xivo.xucami.ami

import java.beans.PropertyChangeEvent

import akka.actor.PoisonPill
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.asteriskjava.live.AsteriskServer
import org.mockito.Matchers.anyObject
import org.mockito.Mockito.{timeout, verify}
import org.scalatest.BeforeAndAfter
import org.scalatest.mock.MockitoSugar
import xivo.xucami.models.{CallerId, Channel, ChannelState}

class LiveServerConnectorSpec extends TestKitSpec("LiveServerConnectorSpec")
  with MockitoSugar
  with BeforeAndAfter {

  class Helper() {
    val asteriskServer = mock[AsteriskServer]
    val channelManager = TestProbe()

    def actor() = {
      val sa = TestActorRef(new LiveServerConnector(channelManager.ref, asteriskServer))
      (sa, sa.underlyingActor)
    }
  }

  override def afterAll {
    system.shutdown()
  }

  "LiveServerConnector" should {

    "initialize AsteriskServer" in new Helper() {
      val (ref, a) = actor()
      verify(asteriskServer, timeout(100)).initialize()
      verify(asteriskServer, timeout(100)).addAsteriskServerListener(anyObject[AsteriskLiveListener])
    }

    "stop the server if the actor is terminated" in new Helper() {
      val (ref, a) = actor()
      val probe = TestProbe()
      probe watch ref
      verify(asteriskServer, timeout(100)).initialize()
      verify(asteriskServer, timeout(100)).addAsteriskServerListener(anyObject[AsteriskLiveListener])
      ref ! PoisonPill

      probe expectTerminated ref
      verify(asteriskServer).shutdown()
    }

    "Send new channel notifications to the Channel Manager" in new Helper() {
      val (ref, _) = actor()
      val msg = Channel("id", "name", CallerId("name", "number"),"linkedId", ChannelState.UNITIALIZED)
      ref ! msg
      channelManager expectMsg msg
    }

    "Send propertyChanges to the Channel Manager" in new Helper() {
      val (ref, _) = actor()
      val msg = mock[PropertyChangeEvent]
      ref ! msg
      channelManager expectMsg msg
    }

  }
}
