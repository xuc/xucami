package services

import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.asteriskjava.manager.action.{OriginateAction, PauseMonitorAction, SetVarAction}
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.specs2.mock.mockito.ArgumentCapture
import services.XucAmiBus._
import services.channel.ChannelManager

class AmiRequestManagerSpec extends TestKitSpec("SetVarManagerSpec") with MockitoSugar {
  class Helper() {
    val amiBus = mock[XucAmiBus]

    def actor() = {
      val sa = TestActorRef(new AmiRequestManager(amiBus))
      (sa, sa.underlyingActor)
    }
  }

  "AmiRequestManager" should {

    "subscribe to the ami bus for set var action requests and listen requests" in new Helper() {
      val (ref, _) = actor()
      verify(amiBus).subscribe(ref, AmiType.AmiRequest)
    }

    "send set variable request to ami bus" in new Helper {
      val (ref, _) = actor()

      val action = new SetVarAction("VarName","VarValue")


      ref ! SetVarRequest(SetVarActionRequest("VarName","VarValue"))

      val arg = new ArgumentCapture[AmiAction]()
      verify(amiBus).publish(arg.capture)

      arg.value.message match {
        case setvar: SetVarAction =>
          setvar.getVariable shouldBe "VarName"
          setvar.getValue shouldBe "VarValue"
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }
    }
    "send listen request to ami bus" in new Helper {
      val (ref, _) = actor()

      val (listener,listened)=("SIP/lkkl","SIP/khkhjd")

      val action = new ListenActionRequest(listener,listened)


      ref ! ListenRequest(action)


      val arg = new ArgumentCapture[AmiAction]()
      verify(amiBus).publish(arg.capture)

      arg.value.message match {
        case originate: OriginateAction =>
          originate.getApplication shouldBe "ChanSpy"
          originate.getAsync shouldBe true
          originate.getChannel shouldBe listener
          originate.getData shouldBe s"$listened,bds"
          originate.getCallerId shouldBe "Listen"
      }
    }
    "publish ami action from request" in new Helper {
      val (ref, _) = actor()
      val amiActionRequest = mock[AmiActionRequest]
      val amiRealAction = mock[XucManagerAction]

      stub(amiActionRequest.buildAction()).toReturn(amiRealAction)

      ref ! AmiRequest(amiActionRequest)

      verify(amiBus).publish(AmiAction(amiRealAction))

    }
  }

  }
